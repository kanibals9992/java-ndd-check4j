package name.didier.david.check4j;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;

public class FluentCollectionCheckerTest
        extends AbstractFluentCheckerTestBase<FluentCollectionChecker<Collection<Object>, Object>, Collection<Object>> {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isNotEmpty_should_pass_if_not_empty() {
        newChecker().isNotEmpty();
    }

    @Test
    public void isNotEmpty_should_return_self() {
        FluentCollectionChecker<Collection<Object>, Object> checker = newChecker();
        assertThat(checker.isNotEmpty()).isSameAs(checker);
    }

    @Test
    public void isNotEmpty_result_should_not_be_casted() {
        List<String> stringList = newArrayList("some string");
        FluentCollectionChecker<List<String>, String> checkerList = new FluentCollectionChecker<>(stringList);
        List<String> uncastedStrings = checkerList.getActual();
        assertThat(uncastedStrings).isSameAs(stringList);

        Set<String> stringSet = newHashSet("some string");
        FluentCollectionChecker<Set<String>, String> checkerSet = new FluentCollectionChecker<>(stringSet);
        Set<String> uncastedStringSet = checkerSet.getActual();
        assertThat(uncastedStringSet).isSameAs(stringSet);
    }

    @Test
    public void isNotEmpty_should_fail_if_null() {
        FluentCollectionChecker<Collection<Object>, Object> checker = newChecker(null);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    public void isNotEmpty_should_fail_if_null_with_pName() {
        FluentCollectionChecker<Collection<Object>, Object> checker = newChecker(null).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    public void isNotEmpty_should_fail_if_empty() {
        FluentCollectionChecker<Collection<Object>, Object> checker = newChecker(newArrayList());
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    @Test
    public void isNotEmpty_should_fail_if_empty_with_pName() {
        FluentCollectionChecker<Collection<Object>, Object> checker = newChecker(newArrayList()).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Collection<Object> newActual() {
        return newArrayList(new Object());
    }

    @Override
    protected FluentCollectionChecker<Collection<Object>, Object> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected FluentCollectionChecker<Collection<Object>, Object> newChecker(final Collection<Object> actual) {
        return new FluentCollectionChecker<>(actual);
    }
}
