package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.io.File;
import org.junit.jupiter.api.Test;

public class FluentFileCheckerTest
        extends AbstractFluentCheckerTestBase<FluentFileChecker, File> {

    private static final File NOT_EXISTING = new File("some/path/that/is/really/unlikely/to/exist");
    private static final File EXISTING_DIRECTORY = new File("src/test/resources");
    private static final File EXISTING_FILE = new File("src/test/resources/logback-test.xml");

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void exists_should_return_self() {
        FluentFileChecker checkerD = newChecker(EXISTING_DIRECTORY);
        assertThat(checkerD.exists()).isEqualTo(checkerD);

        FluentFileChecker checkerF = newChecker(EXISTING_FILE);
        assertThat(checkerF.exists()).isEqualTo(checkerF);
    }

    @Test
    public void exists_should_pass_if_file_exists() {
        newChecker(EXISTING_DIRECTORY).exists();
        newChecker(EXISTING_DIRECTORY).as(P_NAME).exists();

        newChecker(EXISTING_FILE).exists();
        newChecker(EXISTING_FILE).as(P_NAME).exists();
    }

    @Test
    public void exists_should_fail_if_file_does_not_exist() {
        try {
            newChecker(NOT_EXISTING).exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing file");
        }

        try {
            newChecker(NOT_EXISTING).as(P_NAME).exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isFile_should_return_self() {
        FluentFileChecker checker = newChecker(EXISTING_FILE);
        assertThat(checker.isFile()).isEqualTo(checker);
    }

    @Test
    public void isFile_should_pass_if_file_is_a_regular_file() {
        newChecker(EXISTING_FILE).isFile();
        newChecker(EXISTING_FILE).as(P_NAME).isFile();
    }

    @Test
    public void isFile_should_fail_if_file_is_not_a_regular_file() {
        try {
            newChecker(NOT_EXISTING).isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        try {
            newChecker(NOT_EXISTING).as(P_NAME).isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }

        try {
            newChecker(EXISTING_DIRECTORY).isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        try {
            newChecker(EXISTING_DIRECTORY).as(P_NAME).isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isDirectory_should_return_self() {
        FluentFileChecker checker = newChecker(EXISTING_DIRECTORY);
        assertThat(checker.isDirectory()).isEqualTo(checker);
    }

    @Test
    public void isDirectory_should_pass_if_file_is_a_regular_directory() {
        newChecker(EXISTING_DIRECTORY).isDirectory();
        newChecker(EXISTING_DIRECTORY).as(P_NAME).isDirectory();
    }

    @Test
    public void isDirectory_should_fail_if_file_is_not_a_regular_directory() {
        try {
            newChecker(NOT_EXISTING).isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        try {
            newChecker(NOT_EXISTING).as(P_NAME).isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }
        try {
            newChecker(EXISTING_FILE).isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        try {
            newChecker(EXISTING_FILE).as(P_NAME).isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected File newActual() {
        return EXISTING_DIRECTORY;
    }

    @Override
    protected FluentFileChecker newChecker() {
        return new FluentFileChecker(newActual());
    }

    @Override
    protected FluentFileChecker newChecker(File actual) {
        return new FluentFileChecker(actual);
    }
}
