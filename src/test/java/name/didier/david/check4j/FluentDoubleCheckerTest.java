package name.didier.david.check4j;

public class FluentDoubleCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentDoubleChecker, Double> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentDoubleChecker newChecker(final Double actual) {
        return new FluentDoubleChecker(actual);
    }

    @Override
    protected Double numberN() {
        return -1.0;    }

    @Override
    protected Double numberZ() {
        return 0.0;
    }

    @Override
    protected Double numberP() {
        return 1.0;
    }
}
