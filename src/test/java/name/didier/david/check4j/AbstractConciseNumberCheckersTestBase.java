package name.didier.david.check4j;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_LESS;

import org.junit.jupiter.api.Test;

public abstract class AbstractConciseNumberCheckersTestBase<N extends Number>
        extends AbstractConciseCheckersTestBase {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkNegative_should_pass_if_strictly_negative_then_return_parameter() {
        assertThat(checkNegative(numberN())).isEqualTo(numberN());
        assertThat(checkNegative(numberN(), P_NAME)).isEqualTo(numberN());
    }

    @Test
    public void checkNegative_should_pass_if_zero() {
        assertThat(checkNegative(numberZ())).isEqualTo(numberZ());
        assertThat(checkNegative(numberZ(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkNegative_should_fail_if_strictly_positive() {
        try {
            checkNegative(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkNegative(numberP(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkStrictlyNegative_should_pass_if_strictly_negative_then_return_parameter() {
        assertThat(checkStrictlyNegative(numberN())).isEqualTo(numberN());
        assertThat(checkStrictlyNegative(numberN(), P_NAME)).isEqualTo(numberN());
    }

    @Test
    public void checkStrictlyNegative_should_fail_if_zero() {
        try {
            checkStrictlyNegative(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyNegative(numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyNegative_should_fail_if_strictly_positive() {
        try {
            checkStrictlyNegative(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyNegative(numberP(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkPositive_should_pass_if_strictly_positive_then_return_parameter() {
        assertThat(checkPositive(numberP())).isEqualTo(numberP());
        assertThat(checkPositive(numberP(), P_NAME)).isEqualTo(numberP());
    }

    @Test
    public void checkPositive_should_pass_if_zero() {
        assertThat(checkPositive(numberZ())).isEqualTo(numberZ());
        assertThat(checkPositive(numberZ(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkPositive_should_fail_if_strictly_negative() {
        try {
            checkPositive(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkPositive(numberN(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkStrictlyPositive_should_pass_if_strictly_positive_then_return_parameter() {
        assertThat(checkStrictlyPositive(numberP())).isEqualTo(numberP());
        assertThat(checkStrictlyPositive(numberP(), P_NAME)).isEqualTo(numberP());
    }

    @Test
    public void checkStrictlyPositive_should_fail_if_zero() {
        try {
            checkStrictlyPositive(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyPositive(numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyPositive_should_fail_if_strictly_negative() {
        try {
            checkStrictlyPositive(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyPositive(numberN(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkLessThan_should_pass_if_strictly_less_than_then_return_parameter() {
        assertThat(checkLessThan(numberN(), numberZ())).isEqualTo(numberN());
        assertThat(checkLessThan(numberN(), numberZ(), P_NAME)).isEqualTo(numberN());
    }

    @Test
    public void checkLessThan_should_pass_if_zero() {
        assertThat(checkLessThan(numberZ(), numberZ())).isEqualTo(numberZ());
        assertThat(checkLessThan(numberZ(), numberZ(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkLessThan_should_fail_if_strictly_greater() {
        try {
            checkLessThan(numberP(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkLessThan(numberP(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkStrictlyLessThan_should_pass_if_strictly_less_than_then_return_parameter() {
        assertThat(checkStrictlyLessThan(numberN(), numberZ())).isEqualTo(numberN());
        assertThat(checkStrictlyLessThan(numberN(), numberZ(), P_NAME)).isEqualTo(numberN());
    }

    @Test
    public void checkStrictlyLessThan_should_fail_if_zero() {
        try {
            checkStrictlyLessThan(numberZ(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyLessThan(numberZ(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyLessThan_should_fail_if_strictly_greater() {
        try {
            checkStrictlyLessThan(numberP(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyLessThan(numberP(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkGreaterThan_should_pass_if_strictly_greater_than_then_return_parameter() {
        assertThat(checkGreaterThan(numberP(), numberZ())).isEqualTo(numberP());
        assertThat(checkGreaterThan(numberP(), numberZ(), P_NAME)).isEqualTo(numberP());
    }

    @Test
    public void checkGreaterThan_should_pass_if_zero() {
        assertThat(checkGreaterThan(numberZ(), numberZ())).isEqualTo(numberZ());
        assertThat(checkGreaterThan(numberZ(), numberZ(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkGreaterThan_should_fail_if_strictly_less() {
        try {
            checkGreaterThan(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkGreaterThan(numberN(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkStrictlyGreaterThan_should_pass_if_strictly_greater_than_then_return_parameter() {
        assertThat(checkStrictlyGreaterThan(numberP(), numberZ())).isEqualTo(numberP());
        assertThat(checkStrictlyGreaterThan(numberP(), numberZ(), P_NAME)).isEqualTo(numberP());
    }

    @Test
    public void checkStrictlyGreaterThan_should_fail_if_zero() {
        try {
            checkStrictlyGreaterThan(numberZ(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyGreaterThan(numberZ(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyGreaterThan_should_fail_if_strictly_less() {
        try {
            checkStrictlyGreaterThan(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyGreaterThan(numberN(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkBetween_should_pass_if_strictly_between_then_return_parameter() {
        assertThat(checkBetween(numberZ(), numberN(), numberP())).isEqualTo(numberZ());
        assertThat(checkBetween(numberZ(), numberN(), numberP(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkBetween_should_pass_if_equals_to_minimum_then_return_parameter() {
        assertThat(checkBetween(numberN(), numberN(), numberP())).isEqualTo(numberN());
        assertThat(checkBetween(numberN(), numberN(), numberP(), P_NAME)).isEqualTo(numberN());
    }

    @Test
    public void checkBetween_should_pass_if_equals_to_maximum_then_return_parameter() {
        assertThat(checkBetween(numberP(), numberN(), numberP())).isEqualTo(numberP());
        assertThat(checkBetween(numberP(), numberN(), numberP(), P_NAME)).isEqualTo(numberP());
    }

    @Test
    public void checkBetweenThan_should_fail_if_strictly_less() {
        try {
            checkBetween(numberN(), numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkBetween(numberN(), numberZ(), numberP(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    public void checkBetweenThan_should_fail_if_strictly_greater() {
        try {
            checkBetween(numberP(), numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkBetween(numberP(), numberN(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void checkStrictlyBetween_should_pass_if_strictly_between_then_return_parameter() {
        assertThat(checkStrictlyBetween(numberZ(), numberN(), numberP())).isEqualTo(numberZ());
        assertThat(checkStrictlyBetween(numberZ(), numberN(), numberP(), P_NAME)).isEqualTo(numberZ());
    }

    @Test
    public void checkStrictlyBetween_should_fail_if_equals_to_minimum_then_return_parameter() {
        try {
            checkStrictlyBetween(numberZ(), numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyBetween(numberZ(), numberZ(), numberP(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyBetween_should_fail_if_equals_to_maximum_then_return_parameter() {
        try {
            checkStrictlyBetween(numberZ(), numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyBetween(numberZ(), numberN(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    public void checkStrictlyBetweenThan_should_fail_if_strictly_less() {
        try {
            checkStrictlyBetween(numberN(), numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyBetween(numberN(), numberZ(), numberP(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    public void checkStrictlyBetweenThan_should_fail_if_strictly_greater() {
        try {
            checkStrictlyBetween(numberP(), numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyBetween(numberP(), numberN(), numberZ(), P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract N checkNegative(N numberN);

    protected abstract N checkNegative(N numberN, String parameterName);

    protected abstract N checkStrictlyNegative(N numberN);

    protected abstract N checkStrictlyNegative(N numberN, String parameterName);

    protected abstract N checkPositive(N numberN);

    protected abstract N checkPositive(N numberN, String parameterName);

    protected abstract N checkStrictlyPositive(N numberN);

    protected abstract N checkStrictlyPositive(N numberN, String parameterName);

    protected abstract N checkLessThan(N numberN, Number max);

    protected abstract N checkLessThan(N numberN, Number max, String parameterName);

    protected abstract N checkStrictlyLessThan(N numberN, Number max);

    protected abstract N checkStrictlyLessThan(N numberN, Number max, String parameterName);

    protected abstract N checkGreaterThan(N numberN, Number min);

    protected abstract N checkGreaterThan(N numberN, Number min, String parameterName);

    protected abstract N checkStrictlyGreaterThan(N numberN, Number min);

    protected abstract N checkStrictlyGreaterThan(N numberN, Number min, String parameterName);

    protected abstract N checkBetween(N numberN, Number min, Number max);

    protected abstract N checkBetween(N numberN, Number min, Number max, String parameterName);

    protected abstract N checkStrictlyBetween(N numberN, Number min, Number max);

    protected abstract N checkStrictlyBetween(N numberN, Number min, Number max, String parameterName);

    /** Negative as number. */
    protected abstract N numberN();

    /** Zero as number. */
    protected abstract N numberZ();

    /** Positive as number. */
    protected abstract N numberP();

    /** Negative as string. */
    protected String stringN() {
        return numberN().toString();
    }

    /** Zero as string. */
    protected String stringZ() {
        return numberZ().toString();
    }

    /** Positive as string. */
    protected String stringP() {
        return numberP().toString();
    }
}
