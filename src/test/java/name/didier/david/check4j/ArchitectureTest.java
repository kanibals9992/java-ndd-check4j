package name.didier.david.check4j;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;
import static com.tngtech.archunit.library.GeneralCodingRules.*;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

public class ArchitectureTest {

    public abstract static class AbstractArchitectureTest {

        @ArchTest
        static ArchRule do_not_access_standard_streams = NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
        @ArchTest
        static ArchRule do_not_throw_generic_exceptions = NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;
        @ArchTest
        static ArchRule do_not_use_java_util_logging = NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;
        @ArchTest
        static ArchRule do_not_use_joda_time = NO_CLASSES_SHOULD_USE_JODATIME;
    }

    @AnalyzeClasses(packages = "name.didier.david.check4j", importOptions = {ImportOption.DoNotIncludeTests.class})
    public static class MainArchitectureTest
            extends AbstractArchitectureTest {

        @ArchTest
        static ArchRule do_not_use_any_external_library =
                classes().that().resideInAPackage("name.didier.david.check4j..")
                        .should().onlyDependOnClassesThat().resideInAnyPackage(
                                "name.didier.david.check4j..",
                                "java.."
                        );
    }

    @AnalyzeClasses(packages = "name.didier.david.check4j", importOptions = {ImportOption.OnlyIncludeTests.class})
    public static class TestArchitectureTest
            extends AbstractArchitectureTest {

        @ArchTest
        static ArchRule do_not_use_junit_4 =
                noClasses().that().resideInAPackage("name.didier.david.check4j..")
                        .should().dependOnClassesThat().haveFullyQualifiedName("org.junit.Test");
    }
}
