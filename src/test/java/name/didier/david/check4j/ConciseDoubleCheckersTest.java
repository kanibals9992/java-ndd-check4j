package name.didier.david.check4j;

public class ConciseDoubleCheckersTest
        extends AbstractConciseNumberCheckersTestBase<Double> {

    @Override
    protected Double checkNegative(final Double numberA) {
        return ConciseCheckers.checkNegative(numberA);
    }

    @Override
    protected Double checkNegative(final Double numberA, final String pName) {
        return ConciseCheckers.checkNegative(numberA, pName);
    }

    @Override
    protected Double checkStrictlyNegative(final Double numberN) {
        return ConciseCheckers.checkStrictlyNegative(numberN);
    }

    @Override
    protected Double checkStrictlyNegative(final Double numberN, final String pName) {
        return ConciseCheckers.checkStrictlyNegative(numberN, pName);
    }

    @Override
    protected Double checkPositive(final Double numberN) {
        return ConciseCheckers.checkPositive(numberN);
    }

    @Override
    protected Double checkPositive(final Double numberN, final String pName) {
        return ConciseCheckers.checkPositive(numberN, pName);
    }

    @Override
    protected Double checkStrictlyPositive(final Double numberN) {
        return ConciseCheckers.checkStrictlyPositive(numberN);
    }

    @Override
    protected Double checkStrictlyPositive(final Double numberN, final String pName) {
        return ConciseCheckers.checkStrictlyPositive(numberN, pName);
    }

    @Override
    protected Double checkLessThan(final Double numberN, final Number max) {
        return ConciseCheckers.checkLessThan(numberN, max);
    }

    @Override
    protected Double checkLessThan(final Double numberN, final Number max, final String pName) {
        return ConciseCheckers.checkLessThan(numberN, max, pName);
    }

    @Override
    protected Double checkStrictlyLessThan(final Double numberN, final Number max) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max);
    }

    @Override
    protected Double checkStrictlyLessThan(final Double numberN, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max, pName);
    }

    @Override
    protected Double checkGreaterThan(final Double numberN, final Number min) {
        return ConciseCheckers.checkGreaterThan(numberN, min);
    }

    @Override
    protected Double checkGreaterThan(final Double numberN, final Number min, final String pName) {
        return ConciseCheckers.checkGreaterThan(numberN, min, pName);
    }

    @Override
    protected Double checkStrictlyGreaterThan(final Double numberN, final Number min) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min);
    }

    @Override
    protected Double checkStrictlyGreaterThan(final Double numberN, final Number min, final String pName) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min, pName);
    }

    @Override
    protected Double checkBetween(final Double numberN, final Number min, final Number max) {
        return ConciseCheckers.checkBetween(numberN, min, max);
    }

    @Override
    protected Double checkBetween(final Double numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkBetween(numberN, min, max, pName);
    }

    @Override
    protected Double checkStrictlyBetween(final Double numberN, final Number min, final Number max) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max);
    }

    @Override
    protected Double checkStrictlyBetween(final Double numberN, final Number min, final Number max,
            final String pName) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max, pName);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Double numberN() {
        return -1.0;
    }

    @Override
    protected Double numberZ() {
        return 0.0;
    }

    @Override
    protected Double numberP() {
        return 1.0;
    }

}
