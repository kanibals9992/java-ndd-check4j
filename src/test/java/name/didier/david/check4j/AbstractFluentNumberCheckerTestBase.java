package name.didier.david.check4j;

import static java.lang.String.format;
import static name.didier.david.check4j.NumberCheckersUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.Test;

public abstract class AbstractFluentNumberCheckerTestBase<C extends AbstractFluentNumberChecker<C, A>, A extends Number>
        extends AbstractFluentCheckerTestBase<C, A> {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isNegative_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isNegative()).isEqualTo(checker);
    }

    @Test
    public void isNegative_should_pass_if_strictly_negative() {
        newChecker(numberN()).isNegative();
        newChecker(numberN()).as(P_NAME).isNegative();
    }

    @Test
    public void isNegative_should_pass_if_zero() {
        newChecker(numberZ()).isNegative();
        newChecker(numberZ()).as(P_NAME).isNegative();
    }

    @Test
    public void isNegative_should_fail_if_strictly_positive() {
        try {
            newChecker(numberP()).isNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            newChecker(numberP()).as(P_NAME).isNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isStrictlyNegative_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberN());
        assertThat(checker.isStrictlyNegative()).isEqualTo(checker);
    }

    @Test
    public void isStrictlyNegative_should_pass_if_strictly_negative() {
        newChecker(numberN()).isStrictlyNegative();
        newChecker(numberN()).as(P_NAME).isStrictlyNegative();
    }

    @Test
    public void isStrictlyNegative_should_fail_if_zero() {
        try {
            newChecker(numberZ()).isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }
        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    public void isStrictlyNegative_should_fail_if_strictly_positive() {
        try {
            newChecker(numberP()).isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            newChecker(numberP()).as(P_NAME).isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isPositive_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isPositive()).isEqualTo(checker);
    }

    @Test
    public void isPositive_should_pass_if_strictly_positive() {
        newChecker(numberP()).isPositive();
        newChecker(numberP()).as(P_NAME).isPositive();
    }

    @Test
    public void isPositive_should_pass_if_zero() {
        newChecker(numberZ()).isPositive();
        newChecker(numberZ()).as(P_NAME).isPositive();
    }

    @Test
    public void isPositive_should_fail_if_strictly_negative() {
        try {
            newChecker(numberN()).isPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            newChecker(numberN()).as(P_NAME).isPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isStrictlyPositive_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberP());
        assertThat(checker.isStrictlyPositive()).isEqualTo(checker);
    }

    @Test
    public void isStrictlyPositive_should_pass_if_strictly_positive() {
        newChecker(numberP()).isStrictlyPositive();
        newChecker(numberP()).as(P_NAME).isStrictlyPositive();
    }

    @Test
    public void isStrictlyPositive_should_fail_if_zero() {
        try {
            newChecker(numberZ()).isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }
        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    public void isStrictlyPositive_should_fail_if_strictly_negative() {
        try {
            newChecker(numberN()).isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            newChecker(numberN()).as(P_NAME).isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isLessThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isLessThan(numberP())).isEqualTo(checker);
    }

    @Test
    public void isLessThan_should_pass_if_strictly_less() {
        newChecker(numberZ()).isLessThan(numberP());
        newChecker(numberZ()).as(P_NAME).isLessThan(numberP());
    }

    @Test
    public void isLessThan_should_pass_if_equals() {
        newChecker(numberZ()).isLessThan(numberZ());
        newChecker(numberZ()).as(P_NAME).isLessThan(numberZ());
    }

    @Test
    public void isLessThan_should_fail_if_strictly_greater() {
        try {
            newChecker(numberZ()).isLessThan(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringN(), stringZ()));
        }

        try {
            newChecker(numberZ()).as(P_NAME).isLessThan(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringN(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isStrictlyLessThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isStrictlyLessThan(numberP())).isEqualTo(checker);
    }

    @Test
    public void isStrictlyLessThan_should_pass_if_strictly_less() {
        newChecker(numberZ()).isStrictlyLessThan(numberP());
        newChecker(numberZ()).as(P_NAME).isStrictlyLessThan(numberP());
    }

    @Test
    public void isStrictlyLessThan_should_fail_if_equals() {
        try {
            newChecker(numberZ()).isStrictlyLessThan(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }
        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyLessThan(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    public void isStrictlyLessThan_should_fail_if_strictly_greater() {
        try {
            newChecker(numberZ()).isStrictlyLessThan(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringN(), stringZ()));
        }

        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyLessThan(numberN());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringN(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isGreaterThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isGreaterThan(numberN())).isEqualTo(checker);
    }

    @Test
    public void isGreaterThan_should_pass_if_strictly_greater() {
        newChecker(numberZ()).isGreaterThan(numberN());
        newChecker(numberZ()).as(P_NAME).isGreaterThan(numberN());
    }

    @Test
    public void isGreaterThan_should_pass_if_equals() {
        newChecker(numberZ()).isGreaterThan(numberZ());
        newChecker(numberZ()).as(P_NAME).isGreaterThan(numberZ());
    }

    @Test
    public void isGreaterThan_should_fail_if_strictly_less() {
        try {
            newChecker(numberZ()).isGreaterThan(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringP(), stringZ()));
        }

        try {
            newChecker(numberZ()).as(P_NAME).isGreaterThan(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringP(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isStrictlyGreaterThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isStrictlyGreaterThan(numberN())).isEqualTo(checker);
    }

    @Test
    public void isStrictlyGreaterThan_should_pass_if_strictly_greater() {
        newChecker(numberZ()).isStrictlyGreaterThan(numberN());
        newChecker(numberZ()).as(P_NAME).isStrictlyGreaterThan(numberN());
    }

    @Test
    public void isStrictlyGreaterThan_should_fail_if_equals() {
        try {
            newChecker(numberZ()).isStrictlyGreaterThan(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }
        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyGreaterThan(numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    public void isStrictlyGreaterThan_should_fail_if_strictly_less() {
        try {
            newChecker(numberZ()).isStrictlyGreaterThan(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringP(), stringZ()));
        }

        try {
            newChecker(numberZ()).as(P_NAME).isStrictlyGreaterThan(numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringP(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isBetween_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isBetween(numberN(), numberP())).isEqualTo(checker);
    }

    @Test
    public void isBetween_should_pass_if_strictly_between() {
        newChecker(numberZ()).isBetween(numberN(), numberP());
        newChecker(numberZ()).as(P_NAME).isBetween(numberN(), numberP());
    }

    @Test
    public void isBetween_should_pass_if_equals_to_minimum() {
        newChecker(numberN()).isBetween(numberN(), numberP());
        newChecker(numberN()).as(P_NAME).isBetween(numberN(), numberP());
    }

    @Test
    public void isBetween_should_pass_if_equals_to_maximum() {
        newChecker(numberP()).isBetween(numberN(), numberP());
        newChecker(numberP()).as(P_NAME).isBetween(numberN(), numberP());
    }

    @Test
    public void isBetweenThan_should_fail_if_strictly_less() {
        try {
            newChecker(numberN()).isBetween(numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            newChecker(numberN()).as(P_NAME).isBetween(numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    public void isBetweenThan_should_fail_if_strictly_greater() {
        try {
            newChecker(numberP()).isBetween(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            newChecker(numberP()).as(P_NAME).isBetween(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void isStrictlyBetween_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(numberZ());
        assertThat(checker.isStrictlyBetween(numberN(), numberP())).isEqualTo(checker);
    }

    @Test
    public void isStrictlyBetween_should_pass_if_strictly_between() {
        newChecker(numberZ()).isStrictlyBetween(numberN(), numberP());
        newChecker(numberZ()).as(P_NAME).isStrictlyBetween(numberN(), numberP());
    }

    @Test
    public void isStrictlyBetween_should_fail_if_equals_to_minimum() {
        try {
            newChecker(numberN()).isStrictlyBetween(numberN(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringN(), stringN()));
        }

        try {
            newChecker(numberN()).as(P_NAME).isStrictlyBetween(numberN(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringN(), stringN()));
        }
    }

    @Test
    public void isStrictlyBetween_should_fail_if_equals_to_maximum() {
        try {
            newChecker(numberP()).isStrictlyBetween(numberN(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringP(), stringP()));
        }

        try {
            newChecker(numberP()).as(P_NAME).isStrictlyBetween(numberN(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringP(), stringP()));
        }
    }

    @Test
    public void isStrictlyBetweenThan_should_fail_if_strictly_less() {
        try {
            newChecker(numberN()).isStrictlyBetween(numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            newChecker(numberN()).as(P_NAME).isStrictlyBetween(numberZ(), numberP());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    public void isStrictlyBetweenThan_should_fail_if_strictly_greater() {
        try {
            newChecker(numberP()).isStrictlyBetween(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            newChecker(numberP()).as(P_NAME).isStrictlyBetween(numberN(), numberZ());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected AbstractFluentNumberChecker<C, A> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected abstract AbstractFluentNumberChecker<C, A> newChecker(A actual);

    @Override
    protected A newActual() {
        return numberZ();
    }

    /** Negative as number. */
    protected abstract A numberN();

    /** Zero as number. */
    protected abstract A numberZ();

    /** Positive as number. */
    protected abstract A numberP();

    /** Negative as string. */
    protected String stringN() {
        return numberN().toString();
    }

    /** Zero as string. */
    protected String stringZ() {
        return numberZ().toString();
    }

    /** Positive as string. */
    protected String stringP() {
        return numberP().toString();
    }
}
