# Fluent checkers

Check4J provides fluent constructs to help checking parameters.

## Object checks
 
These checks are inherited by all other checks.

```java
this.myObject = checkThat(anObject).as("myObject").isNotNull().thenAssign();
this.myObject = checkThat(anObject).as("myObject").isInstanceOf(Object.class).thenAssign();
```

## String checks
 
```java
this.myString = checkThat(aString).as("myString").isNotEmpty().thenAssign();
this.myString = checkThat(aString).as("myString").isNotBlank().thenAssign();
```

## Number checks
 
Number checks are currently implemented for `Integer`, `Long`, `Float` and `Double`.

```java
this.myInteger = checkThat(anInt).as("myInteger").isNegative().thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isStrictlyNegative().thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isPositive().thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isStrictlyPositive().thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isLessThan(1).thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isStrictlyLessThan(1).thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isGreaterThan(1).thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isStrictlyGreaterThan(1).thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isBetween(-1, 1).thenAssign();
this.myInteger = checkThat(anInt).as("myInteger").isStrictlyBetween(-1, 1).thenAssign();
```

## Array checks
 
```java
this.myArray = checkThat(anArray).as("myArray").isNotEmpty().thenAssign();
```

## Collection checks
 
```java
this.myCollection = checkThat(aCollection).as("myCollection").isNotEmpty().thenAssign();
```
