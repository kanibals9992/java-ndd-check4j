package name.didier.david.check4j;

import static java.lang.String.format;

/**
 * Base class for all checkers.
 *
 * @param <A> the type of the "actual" value.
 * @param <C> the "self" type of this checker class.
 */
public abstract class AbstractFluentChecker<C extends AbstractFluentChecker<C, A>, A>
        implements FluentChecker<C, A> {

    /** The actual value to check. */
    protected final A actual;
    /** This object (self). */
    protected final C myself;
    /** the name of the parameter. */
    private String name;

    /**
     * Abstract constructor.
     *
     * @param actual   the actual value to check.
     * @param selfType {@code this} checker object.
     */
    @SuppressWarnings("unchecked")
    protected AbstractFluentChecker(final A actual, final Class<?> selfType) {
        // We prefer not to use Class<? extends C> selfType because it would force inherited constructor to cast with a
        // compiler warning. Let's keep compiler warning internal (when we can) and not expose them to our end users.
        this.myself = (C) selfType.cast(this);
        this.actual = actual;
    }

    @Override
    public C as(final String parameterName) {
        this.name = parameterName;
        return myself;
    }

    @Override
    public A thenAssign() {
        return actual;
    }

    @Override
    public C isNotNull() {
        if (actual == null) {
            throw expectedParameterTo("not be null");
        }
        return myself;
    }

    @Override
    public C isInstanceOf(final Class<?> type) {
        isNotNull();
        if (!type.isInstance(actual)) {
            String expectedType = type.getName();
            String actualType = actual.getClass().getName();
            throw expectedParameterTo("be an instance of <%s>\nbut was instance of <%s>", expectedType, actualType);
        }
        return myself;
    }

    /**
     * Returns the actual value to check.
     *
     * @return the actual value to check.
     */
    protected A getActual() {
        return actual;
    }

    /**
     * Returns the name of the parameter.
     *
     * @return the name of the parameter.
     */
    protected String getName() {
        return this.name;
    }

    /**
     * Returns a new {@link IllegalArgumentException} with a custom message. The message is prefixed with the name of
     * the parameter if set.
     *
     * @param format the string format of the message.
     * @param args   the optional arguments of the string format.
     * @return a new {@link IllegalArgumentException} with a custom message.
     */
    @SuppressWarnings("AnnotateFormatMethod")
    protected IllegalArgumentException expectedParameterTo(final String format, final Object... args) {
        String prefix;

        if (getName() == null || getName().isEmpty()) {
            prefix = "Expected parameter to ";
        } else {
            prefix = format("Expected parameter '%s' to ", getName());
        }

        return new IllegalArgumentException(prefix + format(format, args));
    }
}
