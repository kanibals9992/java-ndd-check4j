package name.didier.david.check4j;

/**
 * Checking methods for array of {@link Object}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Object[])}</code>.
 *
 * @param <A> the type of the "actual" value.
 */
public class FluentObjectArrayChecker<A>
        extends AbstractFluentChecker<FluentObjectArrayChecker<A>, A[]> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentObjectArrayChecker(final A[] actual) {
        super(actual, FluentObjectArrayChecker.class);
    }

    /**
     * Verifies that the actual value is not {@code null} nor empty.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or empty.
     */
    public FluentObjectArrayChecker<A> isNotEmpty() {
        isNotNull();
        if (actual.length == 0) {
            throw expectedParameterTo("not be empty");
        }
        return myself;
    }
}
