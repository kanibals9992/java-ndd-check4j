package name.didier.david.check4j;

import static name.didier.david.check4j.FluentCheckers.checkThat;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;

/**
 * Single methods for checking different data types. The purpose of this class is to make code more concise, actually
 * wrapping {@link FluentCheckers}. For example:
 *
 * <pre>
 * {@link ConciseCheckers#checkNotNull(Object) checkNotNull}(param1, "param1");
 * </pre>
 *
 * @see FluentCheckers
 */
public class ConciseCheckers {

    /**
     * Default constructor.
     */
    protected ConciseCheckers() {
        super();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is not {@code null}.
     *
     * @param <T>       the type of the parameter.
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null}.
     */
    public static <T> T checkNotNull(final T parameter) {
        return checkThat(parameter).isNotNull().thenAssign();
    }

    /**
     * Checks that the specified parameter is not {@code null}.
     *
     * @param <T>       the type of the parameter.
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null}.
     */
    public static <T> T checkNotNull(final T parameter, final String pName) {
        return checkThat(parameter).as(pName).isNotNull().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is an instance of the given type.
     *
     * @param <C>       the type of the parameter to check (C as class).
     * @param <S>       the type of the subclass (S as subclass).
     * @param parameter the value of the parameter.
     * @param type      the type to check the actual value against.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or is not an instance of the given
     *                                  type.
     */
    @SuppressWarnings("unchecked")
    public static <C, S extends C> S checkInstanceOf(final C parameter, final Class<S> type) {
        return (S) checkThat(parameter).isInstanceOf(type).thenAssign();
    }

    /**
     * Checks that the specified parameter is an instance of the given type.
     *
     * @param <C>       the type of the parameter to check (C as class).
     * @param <S>       the type of the subclass (S as subclass).
     * @param parameter the value of the parameter.
     * @param type      the type to check the actual value against.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or is not an instance of the given
     *                                  type.
     */
    @SuppressWarnings("unchecked")
    public static <C, S extends C> S checkInstanceOf(final C parameter, final Class<S> type, final String pName) {
        return (S) checkThat(parameter).as(pName).isInstanceOf(type).thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is not empty.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is empty.
     */
    public static String checkNotEmpty(final String parameter) {
        return checkThat(parameter).isNotEmpty().thenAssign();
    }

    /**
     * Checks that the specified parameter is not empty.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is empty.
     */
    public static String checkNotEmpty(final String parameter, final String pName) {
        return checkThat(parameter).as(pName).isNotEmpty().thenAssign();
    }

    /**
     * Checks that the specified parameter is not blank.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is blank.
     */
    public static String checkNotBlank(final String parameter) {
        return checkThat(parameter).isNotBlank().thenAssign();
    }

    /**
     * Checks that the specified parameter is not blank.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is blank.
     */
    public static String checkNotBlank(final String parameter, final String pName) {
        return checkThat(parameter).as(pName).isNotBlank().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Integer checkNegative(final Integer parameter) {
        return checkThat(parameter).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Integer checkNegative(final Integer parameter, final String pName) {
        return checkThat(parameter).as(pName).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Integer checkStrictlyNegative(final Integer parameter) {
        return checkThat(parameter).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Integer checkStrictlyNegative(final Integer parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Integer checkPositive(final Integer parameter) {
        return checkThat(parameter).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Integer checkPositive(final Integer parameter, final String pName) {
        return checkThat(parameter).as(pName).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Integer checkStrictlyPositive(final Integer parameter) {
        return checkThat(parameter).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Integer checkStrictlyPositive(final Integer parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Integer checkLessThan(final Integer parameter, final Number maximum) {
        return checkThat(parameter).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Integer checkLessThan(final Integer parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Integer checkStrictlyLessThan(final Integer parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Integer checkStrictlyLessThan(final Integer parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Integer checkGreaterThan(final Integer parameter, final Number maximum) {
        return checkThat(parameter).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Integer checkGreaterThan(final Integer parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Integer checkStrictlyGreaterThan(final Integer parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Integer checkStrictlyGreaterThan(final Integer parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Integer checkBetween(final Integer parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Integer checkBetween(final Integer parameter, final Number minimum, final Number maximum,
                                       final String pName) {
        return checkThat(parameter).as(pName).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Integer checkStrictlyBetween(final Integer parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Integer checkStrictlyBetween(final Integer parameter, final Number minimum, final Number maximum,
                                               final String pName) {
        return checkThat(parameter).as(pName).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Long checkNegative(final Long parameter) {
        return checkThat(parameter).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Long checkNegative(final Long parameter, final String pName) {
        return checkThat(parameter).as(pName).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Long checkStrictlyNegative(final Long parameter) {
        return checkThat(parameter).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Long checkStrictlyNegative(final Long parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Long checkPositive(final Long parameter) {
        return checkThat(parameter).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Long checkPositive(final Long parameter, final String pName) {
        return checkThat(parameter).as(pName).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Long checkStrictlyPositive(final Long parameter) {
        return checkThat(parameter).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Long checkStrictlyPositive(final Long parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Long checkLessThan(final Long parameter, final Number maximum) {
        return checkThat(parameter).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Long checkLessThan(final Long parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Long checkStrictlyLessThan(final Long parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Long checkStrictlyLessThan(final Long parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Long checkGreaterThan(final Long parameter, final Number maximum) {
        return checkThat(parameter).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Long checkGreaterThan(final Long parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Long checkStrictlyGreaterThan(final Long parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Long checkStrictlyGreaterThan(final Long parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Long checkBetween(final Long parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Long checkBetween(final Long parameter, final Number minimum, final Number maximum,
                                    final String pName) {
        return checkThat(parameter).as(pName).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Long checkStrictlyBetween(final Long parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Long checkStrictlyBetween(final Long parameter, final Number minimum, final Number maximum,
                                            final String pName) {
        return checkThat(parameter).as(pName).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Float checkNegative(final Float parameter) {
        return checkThat(parameter).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Float checkNegative(final Float parameter, final String pName) {
        return checkThat(parameter).as(pName).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Float checkStrictlyNegative(final Float parameter) {
        return checkThat(parameter).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Float checkStrictlyNegative(final Float parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Float checkPositive(final Float parameter) {
        return checkThat(parameter).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Float checkPositive(final Float parameter, final String pName) {
        return checkThat(parameter).as(pName).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Float checkStrictlyPositive(final Float parameter) {
        return checkThat(parameter).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Float checkStrictlyPositive(final Float parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Float checkLessThan(final Float parameter, final Number maximum) {
        return checkThat(parameter).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Float checkLessThan(final Float parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Float checkStrictlyLessThan(final Float parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Float checkStrictlyLessThan(final Float parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Float checkGreaterThan(final Float parameter, final Number maximum) {
        return checkThat(parameter).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Float checkGreaterThan(final Float parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Float checkStrictlyGreaterThan(final Float parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Float checkStrictlyGreaterThan(final Float parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyGreaterThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Float checkBetween(final Float parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Float checkBetween(final Float parameter, final Number minimum, final Number maximum,
                                     final String pName) {
        return checkThat(parameter).as(pName).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Float checkStrictlyBetween(final Float parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Float checkStrictlyBetween(final Float parameter, final Number minimum, final Number maximum,
                                             final String pName) {
        return checkThat(parameter).as(pName).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Double checkNegative(final Double parameter) {
        return checkThat(parameter).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not negative.
     */
    public static Double checkNegative(final Double parameter, final String pName) {
        return checkThat(parameter).as(pName).isNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Double checkStrictlyNegative(final Double parameter) {
        return checkThat(parameter).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly negative.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly negative.
     */
    public static Double checkStrictlyNegative(final Double parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyNegative().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Double checkPositive(final Double parameter) {
        return checkThat(parameter).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not positive.
     */
    public static Double checkPositive(final Double parameter, final String pName) {
        return checkThat(parameter).as(pName).isPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Double checkStrictlyPositive(final Double parameter) {
        return checkThat(parameter).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly positive.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is {@code null} or not strictly positive.
     */
    public static Double checkStrictlyPositive(final Double parameter, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyPositive().thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Double checkLessThan(final Double parameter, final Number maximum) {
        return checkThat(parameter).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not less than the specified one.
     */
    public static Double checkLessThan(final Double parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Double checkStrictlyLessThan(final Double parameter, final Number maximum) {
        return checkThat(parameter).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly less than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly less than the specified one.
     */
    public static Double checkStrictlyLessThan(final Double parameter, final Number maximum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyLessThan(maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Double checkGreaterThan(final Double parameter, final Number minimum) {
        return checkThat(parameter).isGreaterThan(minimum).thenAssign();
    }

    /**
     * Checks that the specified parameter is greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not greater than the specified one.
     */
    public static Double checkGreaterThan(final Double parameter, final Number minimum, final String pName) {
        return checkThat(parameter).as(pName).isGreaterThan(minimum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Double checkStrictlyGreaterThan(final Double parameter, final Number minimum) {
        return checkThat(parameter).isStrictlyGreaterThan(minimum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly greater than the specified one.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly greater than the specified one.
     */
    public static Double checkStrictlyGreaterThan(final Double parameter, final Number minimum, final String pName) {
        return checkThat(parameter).as(pName).isStrictlyGreaterThan(minimum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Double checkBetween(final Double parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not between the specified ones.
     */
    public static Double checkBetween(final Double parameter, final Number minimum, final Number maximum,
                                      final String pName) {
        return checkThat(parameter).as(pName).isBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Double checkStrictlyBetween(final Double parameter, final Number minimum, final Number maximum) {
        return checkThat(parameter).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    /**
     * Checks that the specified parameter is strictly between the specified ones.
     *
     * @param parameter the value of the parameter.
     * @param minimum   the minimum value the parameter can have.
     * @param maximum   the maximum value the parameter can have.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is not strictly between the specified ones.
     */
    public static Double checkStrictlyBetween(final Double parameter, final Number minimum, final Number maximum,
                                              final String pName) {
        return checkThat(parameter).as(pName).isStrictlyBetween(minimum, maximum).thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is neither null nor empty.
     *
     * @param <T>       the type of the actual value.
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is null or empty.
     */
    public static <T> T[] checkNotEmpty(final T[] parameter) {
        return checkThat(parameter).isNotEmpty().thenAssign();
    }

    /**
     * Checks that the specified parameter is neither null nor empty.
     *
     * @param <T>       the type of the actual value.
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is null or empty.
     */
    public static <T> T[] checkNotEmpty(final T[] parameter, final String pName) {
        return checkThat(parameter).as(pName).isNotEmpty().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified parameter is neither null nor empty.
     *
     * @param <C>       the type of the actual collection.
     * @param <T>       the type of the actual value.
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is null or empty.
     */
    public static <C extends Collection<T>, T> C checkNotEmpty(final C parameter) {
        return checkThat(parameter).isNotEmpty().thenAssign();
    }

    /**
     * Checks that the specified parameter is neither null nor empty.
     *
     * @param <C>       the type of the actual collection.
     * @param <T>       the type of the actual value.
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the value of the parameter is null or empty.
     */
    public static <C extends Collection<T>, T> C checkNotEmpty(final C parameter, final String pName) {
        return checkThat(parameter).as(pName).isNotEmpty().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified file is not null and exists.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or does not exist.
     */
    public static File checkExists(final File parameter) {
        return checkThat(parameter).exists().thenAssign();
    }

    /**
     * Checks that the specified file is not null and exists.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or does not exist.
     */
    public static File checkExists(final File parameter, final String pName) {
        return checkThat(parameter).as(pName).exists().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified file is not null and is an existing regular file.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or is not an existing regular file.
     */
    public static File checkIsFile(final File parameter) {
        return checkThat(parameter).isFile().thenAssign();
    }

    /**
     * Checks that the specified file is not null and is an existing regular file.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or is not an existing regular file.
     */
    public static File checkIsFile(final File parameter, final String pName) {
        return checkThat(parameter).as(pName).isFile().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified file is not null and is an existing regular directory.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or is not an existing regular directory.
     */
    public static File checkIsDirectory(final File parameter) {
        return checkThat(parameter).isDirectory().thenAssign();
    }

    /**
     * Checks that the specified file is not null and is an existing regular directory.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified file is null or is not an existing regular directory.
     */
    public static File checkIsDirectory(final File parameter, final String pName) {
        return checkThat(parameter).as(pName).isDirectory().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified path is not null and exists.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or does not exist.
     */
    public static Path checkExists(final Path parameter) {
        return checkThat(parameter).exists().thenAssign();
    }

    /**
     * Checks that the specified path is not null and exists.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or does not exist.
     */
    public static Path checkExists(final Path parameter, final String pName) {
        return checkThat(parameter).as(pName).exists().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified path is not null and is an existing regular file.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or is not an existing regular file.
     */
    public static Path checkIsFile(final Path parameter) {
        return checkThat(parameter).isFile().thenAssign();
    }

    /**
     * Checks that the specified path is not null and is an existing regular file.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or is not an existing regular file.
     */
    public static Path checkIsFile(final Path parameter, final String pName) {
        return checkThat(parameter).as(pName).isFile().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Checks that the specified path is not null and is an existing regular directory.
     *
     * @param parameter the value of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or is not an existing regular directory.
     */
    public static Path checkIsDirectory(final Path parameter) {
        return checkThat(parameter).isDirectory().thenAssign();
    }

    /**
     * Checks that the specified path is not null and is an existing regular directory.
     *
     * @param parameter the value of the parameter.
     * @param pName     the name of the parameter.
     * @return the value of the parameter.
     * @throws IllegalArgumentException if the specified path is null or is not an existing regular directory.
     */
    public static Path checkIsDirectory(final Path parameter, final String pName) {
        return checkThat(parameter).as(pName).isDirectory().thenAssign();
    }

}
