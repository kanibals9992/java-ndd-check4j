
NDD Check4J
===========

Easy parameters checking with a fluent or a concise API.

The documentation of the master branch is [available on GitLab Pages][ndd-gitlab-pages].

The documentations of the other branches are [available in the GitLab Environments section][ndd-gitlab-environments].
The environments of these branches are accessible in one of the `Available` or `Stopped` tabs and are displayed by clicking on the `Open live environment` button at the right.


Usage
-----

Before NDD Check4J, aka the messy version:

```java
public void checkMyParametersWithoutCheck4J(final String iJustCantBeEmpty, String meNeither) {
    if (iJustCantBeEmpty == null || iJustCantBeEmpty.length() == 0) {
        throw new IllegalArgumentException("iJustCantBeEmpty just can't be empty, told you so!");
    }
    if (meNeither == null || meNeither.length() == 0) {
        throw new IllegalArgumentException("meNeither neither by the way");
    }
    this.iJustCantBeEmpty = iJustCantBeEmpty;
    this.meNeither = meNeither;
}
```

With the **fluent version** of NDD Check4J (see the [documentation](https://ddidier.gitlab.io/java-ndd-check4j/fluent-checkers.html)):

```java
import static name.didier.david.check4j.FluentCheckers.checkThat;

public void checkMyParametersWithCheck4J(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkThat(iJustCantBeEmpty).isNotEmpty().thenAssign();
    this.meNeither        = checkThat(meNeither).as("meNeither").isNotEmpty().thenAssign();
}
```

`checkThat()` creates a new `Checker` upon which you can chain any validation method such as `isNotEmpty()`. If you want the message to reference the parameter name, you must use `as()` since there is no other way to get it. If you want to assign the parameter, which is a quite common thing, you must call `thenAssign()` at the end of the chain, effectively terminating it.


If this is too verbose for you, there is a **concise version** of NDD Check4J (see the [documentation](https://ddidier.gitlab.io/java-ndd-check4j/concise-checkers.html)):

```java
public void checkMyParametersWithCheck4JButBeConcisePlease(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkNotEmpty(iJustCantBeEmpty);
    this.meNeither = checkNotEmpty(meNeither, "meNeither");
}
```

For the time being, only very basic checks are provided, but they should cover at least 80% of real life use cases and other may be added...

Setup
-----

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-check4j</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

Reports
-------

[Maven reports are available](https://ddidier.gitlab.io/java-ndd-check4j/project-reports.html) including CheckStyle, DependencyCheck, JaCoCo, PMD and SpotBugs.

About
-----

Thanks to the [AssertJ](http://joel-costigliola.github.io/assertj/) contributors which inspired this library.

Copyright David DIDIER 2014



[ndd-gitlab-environments]: https://gitlab.com/ddidier/java-ndd-check4j/-/environments
[ndd-gitlab-pages]: https://ddidier.gitlab.io/java-ndd-check4j/
